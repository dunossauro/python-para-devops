# python-para-devops

Curso de Python ministrado para o time de DevOps

## Tópicos iniciais

### Semana 1 - Introdução ao Python
- Tipos de dados
 - Números
 - String
 - Containers
  - Tuplas
  - Listas
  - Sets
  - Dicionários
- Estruturas de repetição
 - For
 - While
- Estruturas de decisão
 - If

### Semana 2 - Estruturas e abstrações
- Funções
  - Criando funções
  - os
  - shutil
- Testes unitários
  - Unittest
  - Mocks


## Tópicos médios

### Semana 3 - Bibliotecas e ambientes
- Bibliotecas de terceiros
  - PyPI
- Como isolar dependências?
  - Virtualenv
  - Pipenv
  - Poetry


### Semana 4 - API Restfull
- Introdução ao Flask
- Flask e WSGI
  - Gunicorn
  - uWSGI


## Tópicos avançados

### Semana 5 - Conectividade
- Paramiko
- VCR
- 
